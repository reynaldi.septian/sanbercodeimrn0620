//soal 1
console.log("===============SOAL 1===============")
function range (startNum, finishNum){ 
    if (!startNum || !finishNum){return -1}
    else {
        var result = []
        if(startNum > finishNum){
            while (finishNum <= startNum ){
                result.push(startNum)
                startNum--
                
            }
        }
        else{
            while (finishNum >= startNum){
                result.push(startNum)
                startNum++
            
        }
    }
    }
    return result
}

console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range()) 

//soal 2
console.log("===============SOAL 2===============")
function rangeWithStep (startNum, finishNum, step=1){ 
    if (!startNum || !finishNum){return -1}
    else {
        var result = []
        if(startNum > finishNum){
            while (finishNum <= startNum ){
                result.push(startNum)
                startNum = startNum-step
                
            }
        }
        else{
            while (finishNum >= startNum){
                result.push(startNum)
                startNum = startNum+step
            
        }
    }
    }
    return result
}

console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4))  

//soal 3
console.log("===============SOAL 3===============")
function sum (startNum, finishNum=startNum, step=1){ 
        var result = 0
        if(startNum > finishNum){
            while (finishNum <= startNum ){
                result=result+startNum
                startNum = startNum-step
                
            }
        }
        else{
            while (finishNum >= startNum){
                result=result+startNum
                startNum = startNum+step
            
        }
    }
    return result
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum()) 

//soal 4
console.log("===============SOAL 4===============")

function datahandling(input){
var length = input.length
for (var i=0;i<length;i++){
console.log('Nomor Id: '+ input[i][0]+"\nNama lengkap :"+ input [i][1]+"\nTTL: "+ input[i][2]+"\nHobi: "+input[i][3])
}

}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
datahandling(input)

//soal 5
console.log("===============SOAL 5===============")
function balikKata(balik){
var hasil = ""
for(var i=balik.length-1;i>=0;i--){
    hasil = hasil+balik[i]
}
return hasil
} 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma 

//soal 6
console.log("===============SOAL 6===============")
function datahandling2(array){
 
     array.splice (1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
     array.splice(4,2,"Pria","SMA Internasional Metro")
     console.log(array)

     var tanggal = input[3].split('/')
     var month = "";

     switch(tanggal[1]){
         case '01':{month = "Januari"; break}
         case '02':{month = "Februari"; break}
         case '03':{month = "Maret"; break}
         case '04':{month = "April"; break}
         case '05':{month = "Mei"; break}
         case '06':{month = "Juni"; break}
         case '07':{month = "Juli"; break}
         case '08':{month = "Agustus"; break}
         case '09':{month = "September"; break}
         case '10':{month = "Oktober"; break}
         case '11':{month = "November"; break}
         case '12':{month = "Desember"; break}
     }
     console.log(month);

     tanggal.sort(function(value1,value2){return value2 - value1});
     console.log(tanggal);

     console.log(array[3].split("/").join("-"));

     console.log(array[1].slice(0, 14));
}
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
datahandling2(input)
